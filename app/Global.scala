import play.api._
import play.api.mvc._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object AccessLoggingFilter extends Filter {

  val accessLogger = Logger("access")

  val fileBlacklist = Array(
    "/assets//js/bootstrap.min.js",
    "/assets/css/bootstrap.min.css",
    "/assets/css/style.css",
    "/assets/images/favicon.ico",
    "/assets/fonts/glyphicons-halflings-regular.woff2")

  def apply(next: (RequestHeader) => Future[Result])(request: RequestHeader): Future[Result] = {
    val resultFuture = next(request)

    resultFuture.foreach(result => {

      if(fileBlacklist contains (request.uri))
      {
        // Don't log static files
      }
      else
      {
        val msg = s"${request.method} ${request.uri} remote-address=${request.remoteAddress}" +
          s" status=${result.header.status}";
        accessLogger.info(msg)
      }
    })

    resultFuture
  }
}

object Global extends WithFilters(AccessLoggingFilter) {

  override def onStart(app: Application) {
    Logger.info("Application has started")
  }

  override def onStop(app: Application) {
    Logger.info("Application has stopped")
  }
}