package controllers

import javax.inject.Inject

import models.User
import play.api.Logger
import play.api.data.Forms._
import play.api.data._
import play.api.data.validation.Constraints
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc._

case class Search(keyword: String)

/**
  * Created by chrisianson on 28/12/2015.
  */
class UserSearchController @Inject()(val messagesApi: MessagesApi) extends Controller with I18nSupport {

  val logger: Logger = Logger("usersearchcontroller")

  val searchForm = Form(
    mapping(
      "keyword" -> text.verifying(Constraints.nonEmpty)
    )(Search.apply)(Search.unapply)
  )

  def index = Action {

    Ok(views.html.search(User.find, searchForm, null))
  }

  def submit = Action { implicit request =>

    searchForm.bindFromRequest.fold(
      formWithErrors => {

        logger.debug(s"Form submission failed with errors - $formWithErrors")
        BadRequest(views.html.search(User.find, formWithErrors, null))
      },
      formData => {

        Ok(views.html.search(User.searchByKeyword(formData.keyword), searchForm, formData.keyword))
      }
    )
  }
}
