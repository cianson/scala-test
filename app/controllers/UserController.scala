package controllers

import javax.inject.Inject

import models.{DB, User}
import play.api.Logger
import play.api.data.Forms._
import play.api.data._
import play.api.data.validation.Constraints
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc._

/**
  * Created by chrisianson on 23/12/2015.
  */
class UserController @Inject()(val messagesApi: MessagesApi) extends Controller with I18nSupport {

  val logger: Logger = Logger("usercontroller")

  var successMessage: String = null

  val userForm = Form(
    mapping(
      "name" -> text.verifying(Constraints.nonEmpty),
      "email" -> email
    )(User.apply)(User.unapply).verifying("The email address you entered already exists, please try again with another email.", f => !User.emailExists(f.email.toLowerCase))
  )

  def index = Action {

    successMessage = null

    Ok(views.html.index(User.find, userForm, successMessage))
  }

  def create = Action { implicit request =>

    userForm.bindFromRequest.fold(
      formWithErrors => {

        var errors = formWithErrors.errorsAsJson

        logger.debug(s"Form submission failed with errors - $errors")

        BadRequest(views.html.index(User.find, formWithErrors, successMessage))
      },
      userData => {

        val user = User.create( User(userData.name, userData.email.toLowerCase) )

        successMessage = "Thanks for registering your details."

        Created(views.html.index(User.find, userForm, successMessage))
      }
    )
  }

  def delete = Action { implicit request =>

    DB.delete("users");

    val message = "Users deleted."

    Ok(views.html.index(User.find, userForm, message))
  }
}
