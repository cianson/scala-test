package controllers

import play.api.mvc._

class Application extends Controller {

  def index = Action {
    Ok("Not used...")
  }

  def untrail(path: String) = Action {
    MovedPermanently("/" + path)
  }
}