package models

import com.mongodb.casbah.commons.MongoDBObject
import play.api.Logger

/**
  * Created by chrisianson on 23/12/2015.
  */
case class User(name: String, email: String)

object User {

  val logger: Logger = Logger("user")

  def buildObject(dbo: MongoDBObject): User = {

    User(dbo.as[String]("name"), dbo.as[String]("email"));
  }

  def find(): scala.collection.mutable.MutableList[models.User] =
  {
    val list = DB.find("users", Map())

    val count = list.length

    logger.debug(s"Total users found in DB - $count")

    list
  }

  def searchByKeyword(keyword: String): scala.collection.mutable.MutableList[models.User] = {

    logger.debug(s"Search users by keyword - $keyword")

    val list = DB.search("users", Map("email" -> keyword))

    val count = list.length

    logger.debug(s"Total users found from search - $count")

    list
  }

  def create(user: User) {

    logger.debug(s"Insert new user - $user")

    DB.insert("users", Map("name" -> user.name, "email" -> user.email))
  }

  def emailExists(email:String): Boolean =
  {
    logger.debug(s"Check if email exists - $email")

    val list = DB.find("users", Map("email" -> email))

    if(list.isEmpty)
    {
      logger.debug(s"Email does not exist - $email")

      return false
    }

    logger.debug(s"Email exists - $email")

    return true
  }
}
