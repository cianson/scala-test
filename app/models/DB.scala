package models

import com.mongodb.casbah.Imports._
import play.api.Logger
import play.api.Play._

import scala.collection.mutable

/**
  * Created by chrisianson on 23/12/2015.
  */
object DB {

  val logger: Logger = Logger("db")

  val mongoClient = MongoClient()

  val db = mongoClient( current.configuration.getString("database.mongoclient").getOrElse("default") )

  def find(collectionName: String, query: Map[String, String]):  scala.collection.mutable.MutableList[models.User] = {

    val collection = db(collectionName)

    val result = collection.find( query )

    var list = mutable.MutableList[models.User]()

    try {

      if(collection.count() > 0)
      {
        val count = result.count();

        for(record <- result) {

          list.+= (models.User.buildObject(record))
        }
      }

    } finally {
      //
    }

    list
  }

  def search(collectionName: String, query: Map[String, String]): scala.collection.mutable.MutableList[models.User] = {

    val collection = db(collectionName)

    val searchString = query.getOrElse("email", null) + ".*";

    val emailQuery = MongoDBObject("email" -> searchString.r)
    val nameQuery = MongoDBObject("name" -> searchString.r)
    val builder = MongoDBList.newBuilder
    builder += emailQuery
    builder += nameQuery

    val result = collection.find( MongoDBObject("$or" -> builder.result) )

    var list = mutable.MutableList[models.User]()

    try {

      if(result.count() > 0)
      {
        for(record <- result) {

          list.+= (models.User.buildObject(record))
        }
      }

    } finally {
      //
    }

    list
  }

  def delete(collectionName: String) {

    logger.debug(s"Delete - $collectionName collection")

    val collection = db(collectionName)

    val query = MongoDBObject()

    try {

      if (collection.count() > 0) {

        val result = collection.remove( query )
      }

    } finally {
      //
    }
  }

  def insert(collectionName: String, inserts: Map[String, String]) {

    val collection = db(collectionName)

    logger.debug(s"Insert new user - $inserts")

    val newUser = MongoDBObject(
      "name" -> inserts.getOrElse("name", null),
      "email" -> inserts.getOrElse("email", null))

    collection.insert( newUser )

    logger.debug(s"New user inserted - $newUser")
  }
}
