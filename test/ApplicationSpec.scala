import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._

import play.api.test._
import play.api.test.Helpers._

/**
 * Add your spec here.
 * You can mock out a whole application including requests, plugins etc.
 * For more information, consult the wiki.
 */
@RunWith(classOf[JUnitRunner])
class ApplicationSpec extends Specification {

  "Application" should {

    "send 404 on a bad request" in new WithApplication{
      val Some(result) = route(FakeRequest(GET, "/boum"))
      status(result) must equalTo(404)
    }

    "render the index page" in new WithApplication{
      val home = route(FakeRequest(GET, "/")).get

      status(home) must equalTo(OK)
      contentType(home) must beSome.which(_ == "text/html")
      contentAsString(home) must contain ("OrangeBus: Scala App")
    }

    "render the search page" in new WithApplication{
      val search = route(FakeRequest(GET, "/user/search")).get

      status(search) must equalTo(OK)
      contentType(search) must beSome.which(_ == "text/html")
      contentAsString(search) must contain ("You can search for users using the form below")
    }

    "delete existing users" in new WithApplication{

      val deleteUser = route(FakeRequest(POST, "/user/delete")).get

      status(deleteUser) must equalTo(OK)
      contentType(deleteUser) must beSome.which(_ == "text/html")
      contentAsString(deleteUser) must not contain (" Registered Users")
    }

    "add a new user" in new WithApplication{

      val addUser = route(FakeRequest(POST, "/user").withFormUrlEncodedBody("name" -> "Tony Soprano", "email" -> "bigt@thebing.com")).get

      status(addUser) must equalTo(201)
      contentType(addUser) must beSome.which(_ == "text/html")
    }

    "try to add an existing user email" in new WithApplication{

      val addExistingUser = route(FakeRequest(POST, "/user").withFormUrlEncodedBody("name" -> "Tony Soprano", "email" -> "bigt@thebing.com")).get

      status(addExistingUser) must equalTo(400)
      contentType(addExistingUser) must beSome.which(_ == "text/html")
      contentAsString(addExistingUser) must contain ("The email address you entered already exists, please try again with another email")
    }
  }
}
